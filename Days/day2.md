# Modules

Modules are containers for multiple resources that are used together. A module consists of a collection of .tf and/or .tf.json files kept together in a directory.

Modules are the main way to package and reuse resource configurations with Terraform.

# The Root Module

Every Terraform configuration has at least one module, known as its root module, which consists of the resources defined in the .tf files in the main working directory.

# Child Modules

A Terraform module (usually the root module of a configuration) can call other modules to include their resources into the configuration. A module that has been called by another module is often referred to as a child module.

Child modules can be called multiple times within the same configuration, and multiple configurations can use the same child module.

# Module structure

Re-usable modules are defined using all of the same configuration language concepts we use in root modules. Most commonly, modules use:

- Input variables to accept values from the calling module.
- Output values to return results to the calling module, which it can then use to populate arguments elsewhere.
- Resources to define one or more infrastructure objects that the module will manage.

To define a module, create a new directory for it and place one or more .tf files inside just as you would do for a root module. Terraform can load modules either from local relative paths or from remote repositories; if a module will be re-used by lots of configurations you may wish to place it in its own version control repository.

# Calling a Child Module

To call a module means to include the contents of that module into the configuration with specific values for its input variables. Modules are called from within other modules using module blocks:

```terraform
module "servers" {
  source = "./app-cluster"

  servers = 5
}
```

# Backends

Each Terraform configuration can specify a backend, which defines where and how operations are performed, where state snapshots are stored, etc.

The rest of this page introduces the concept of backends; the other pages in this section document how to configure and use backends.

- Backend Configuration documents the form of a backend block, which selects and configures a backend for a Terraform configuration.
- This section also includes a page for each of Terraform's built-in backends, documenting its behavior and available settings. See the navigation sidebar for a complete list.

# Using a Backend Block

Backends are configured with a nested backend block within the top-level terraform block:

```terraform
terraform {
  backend "s3" {
    dynamodb_table = "dynamodb-terraform-state-lock"
    bucket         = "descomplicando-terraform-ramon-borges"
    key            = "terraform-test.tfstate"
    region         = "us-east-2"
    encrypt        = true
  }
}
```

There are some important limitations on backend configuration:

- A configuration can only provide one backend block.
- A backend block cannot refer to named values (like input variables, locals, or data source attributes).

# State

Terraform must store state about your managed infrastructure and configuration. This state is used by Terraform to map real world resources to your configuration, keep track of metadata, and to improve performance for large infrastructures.

This state is stored by default in a local file named "terraform.tfstate", but it can also be stored remotely, which works better in a team environment.

Terraform uses this local state to create plans and make changes to your infrastructure. Prior to any operation, Terraform does a refresh to update the state with the real infrastructure.

The primary purpose of Terraform state is to store bindings between objects in a remote system and resource instances declared in your configuration. When Terraform creates a remote object in response to a change of configuration, it will record the identity of that remote object against a particular resource instance, and then potentially update or delete that object in response to future configuration changes.

# State Management Commands

Subcommands:
- list                List resources in the state
- mv                  Move an item in the state
- pull                Pull current state and output to stdout
- push                Update remote state from a local state file
- rm                  Remove instances from the state
- show                Show a resource in the state

# Workspaces

Each Terraform configuration has an associated backend that defines how operations are executed and where persistent data such as the Terraform state are stored.

The persistent data stored in the backend belongs to a workspace. Initially the backend has only one workspace, called "default", and thus there is only one Terraform state associated with that configuration.

Certain backends support multiple named workspaces, allowing multiple states to be associated with a single configuration. The configuration still has only one backend, but multiple distinct instances of that configuration to be deployed without configuring a new backend or changing authentication credentials.

# Terraform Workspace Management Commands

Subcommands:
- new                 Creates a Terraform workspace
- list                Lists all Terraform workspaces
- show                Shows all Terraform workspace
- select              Selects a Terraform workspace
- delete              Deletes a Terraform workspace