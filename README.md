# Uncomplicating Terraform

This repository was created during the course "Descomplicando Terraform" which was free translated from portuguese to english by me :)

The main objectives here are:
- Understand the basic concepts of the tool
- Practice Terraform
- Share the knowledge with other colleagues
- Create a wiki made by me to explain to the future me some topics that I may forget

## List of Contents at Each Day
- [Day 1](Days/day1.md). Topics: HCL, basic commands, providers and variables.
- [Day 2](Days/day2.md). Topics: Modules, backend, state-file (advanced commands), importing resources, workspace.
- [Day 3](Days/day3.md). Topics: Dependency between resources, taint, graph, fmt and validate.
- [Day 4](Days/day4.md). Topics: Conditions, type-constraints and for_each
- [Day 5](Days/day5.md). Topics: Dynamic block and string templates.
