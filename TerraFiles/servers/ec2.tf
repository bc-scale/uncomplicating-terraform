data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "ec2" {
  ami           = data.aws_ami.ubuntu.id
  count         = var.environment == "production" ? 5 : 1
  instance_type = var.environment == "production" ? "t2.medium" : "t2.micro"

  dynamic "ebs_block_device" {
    for_each = var.disks
    content { 
      device_name = ebs_block_device.value["name"]
      volume_size = ebs_block_device.value["size"]
      volume_type = ebs_block_device.value["type"]
    }
  }

  tags = {
    "name"        = "Server Name: ${var.tags.name}"
    "environment" = var.tags.environment
  }

}

resource "aws_eip" "elastic_ips_manager" {
  count    = var.environment == "production" ? 5 : 1
  vpc      = true
  instance = "${element(aws_instance.ec2.*.id,count.index)}"
}

resource "local_file" "ips" {
  content  = templatefile("${path.module}/templates/ips.tmpl", {
    public-ips = "${aws_instance.ec2[*].public_ip}"
  })
  filename = "ips.txt"

  depends_on = [aws_instance.ec2]
}