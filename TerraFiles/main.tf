provider "aws" {
  region  = "us-east-2"
  version = "~> 3.0"
}

terraform {
  backend "s3" {
    dynamodb_table = "dynamodb-terraform-state-lock"
    bucket         = "descomplicando-terraform-ramon-borges"
    key            = "terraform-test.tfstate"
    region         = "us-east-2"
    encrypt        = true
  }
}