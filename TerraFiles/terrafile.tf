module "dynamodb" {
  source = "./dynamodb"
}

module "servers" {
  source      = "./servers"
  environment = var.environment

  tags = {
    "name"        = "Nginx"
    "environment" = var.environment
  }

  disks = [
    {
      name = "/dev/sdg"
      size = 5
      type = "gp2"
    },
    {
      name = "/dev/sdh"
      size = 10
      type = "gp2"
    },
  ]
}

output "elastic_ips" {
  value = module.servers.instances_public_ip
}